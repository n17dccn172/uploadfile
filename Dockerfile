FROM golang:1.14-alpine

WORKDIR /go/src/uploadimages

COPY . .

RUN go mod download

RUN go build -o ./uploadimages

EXPOSE 5000

CMD ["./uploadimages"]

