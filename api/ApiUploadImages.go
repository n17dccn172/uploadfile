package api

import (
	"uploadimages/driver"
	"uploadimages/model"
	"uploadimages/repository/impl"
	"uploadimages/utils/message"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

func ImagesAPI(w http.ResponseWriter, r *http.Request){
	if r.Method == http.MethodPost {
		owner := JWTparseOwner(r.Header.Get("Authorization"))
		fmt.Println("File Upload Endpoint Hit")
		r.ParseMultipartForm(32 << 20)

		//files:= r.MultipartForm.File[""]
		files :=  r.MultipartForm.File["myFile"]
		for _, fh := range files{
			file,err := fh.Open()

			if err!=nil{
				message.ResponseErr(w,http.StatusBadRequest)
				return
			}
			fileBytes, err := ioutil.ReadAll(file)
			if err != nil {
				message.ResponseErr(w,http.StatusBadRequest)
				return
			}
			image := model.Images{
				File: fileBytes,
				Owner: owner,
			}
			err = impl.NewImagesRepo(driver.DB).InsertImages(image)
			if err != nil {
				message.ResponseErr(w,http.StatusRequestTimeout)
				return
			}
			message.ResponseOk(w,"Successfully Uploaded File")
		}
	}
	if r.Method == http.MethodGet{
		vars := r.URL.Query().Get("id")
		if vars != ""{
			id,_ :=  strconv.Atoi(vars)
			image := impl.NewImagesRepo(driver.DB).GetImageByID(id)
			if image==nil {
				message.ResponseErr(w,http.StatusNotFound)
				return
			}
			w.Write(image.File)
		}else{
			owner := JWTparseOwner(r.Header.Get("Authorization"))
			if len(owner)<=0 {
				message.ResponseErr(w,http.StatusBadRequest)
				return
			}
			ids,err := impl.NewImagesRepo(driver.DB).GetImageIdsByOwner(owner)
			if err != nil || len(ids) <=0 {
				message.ResponseErr(w,http.StatusNotFound)
				return
			}
			body, err := json.Marshal(ids)
			if err!=nil{
				message.ResponseErr(w,http.StatusForbidden)
				return
			}
			w.Write(body)
		}
	}
}
