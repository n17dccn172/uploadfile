package model
type Images struct{
	Idimage uint `json:"idimage"`
	Owner string `json:"owner"`
	File []byte `json:"file"`
}