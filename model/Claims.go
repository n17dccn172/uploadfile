package model

import "github.com/dgrijalva/jwt-go"

type Claims struct {
	Name string `json:"name"`
	jwt.StandardClaims
}