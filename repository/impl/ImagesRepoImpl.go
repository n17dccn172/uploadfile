package impl

import (
	"uploadimages/model"
	"uploadimages/repository"
	"database/sql"
)

type ImagesRepoImpl struct {
	Db *sql.DB
}

func NewImagesRepo(db *sql.DB) repository.ImagesRepo{
	return &ImagesRepoImpl {
		Db : db,
	}
}

func (i *ImagesRepoImpl) GetImagesUser(owner string) ([]model.Images, error) {
	images := make([]model.Images, 0)
	insertStatement := `SELECT * FROM images WHERE owner = $1`
	rows, err := i.Db.Query(insertStatement,owner)
	if err != nil {
		return images, err
	}
	defer rows.Close()
	for rows.Next() {
		image := model.Images{}
		err := rows.Scan(&image.Idimage, &image.Owner,&image.File)
		if err != nil {
			break
		}
		images = append(images,image)
	}
	err = rows.Err()
	if err != nil {
		return images, err
	}
	return images, nil
}

func (i *ImagesRepoImpl) InsertImages(image model.Images) (error) {
	insertStatement := `INSERT INTO images (file,owner) VALUES ($1, $2)`
	_, err := i.Db.Exec(insertStatement, image.File, image.Owner)
	if err != nil {
		return err
	}
	return nil
}

func (i *ImagesRepoImpl) GetImageIdsByOwner(owner string) ([]int, error){
	ids := make([]int, 0)
	insertStatement := `SELECT idimage FROM images WHERE owner = $1`
	rows, err := i.Db.Query(insertStatement,owner)
	if err != nil {
		return ids, err
	}
	defer rows.Close()
	for rows.Next() {
		var id int
		err := rows.Scan(&id)
		if err != nil {
			break
		}
		ids = append(ids,id)
	}

	err = rows.Err()
	if err != nil {
		return ids, err
	}

	return ids, nil
}
func (i *ImagesRepoImpl) GetImageByID(id int) *model.Images{
	image := &model.Images{}
	insertStatement := `SELECT * FROM images WHERE idimage = $1`
	rows, err := i.Db.Query(insertStatement,id)

	if err != nil {
		return image
	}
	defer rows.Close()
	if rows.Next() {
		err := rows.Scan(&image.Idimage, &image.Owner,&image.File)
		if err != nil {
			return image
		}
	}
	return image
}