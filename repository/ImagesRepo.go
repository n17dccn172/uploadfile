package repository

import "uploadimages/model"

type ImagesRepo interface {
	InsertImages(imgaes model.Images) error
	GetImagesUser(owner string) ([]model.Images, error)
	GetImageIdsByOwner(owner string) ([]int, error)
	GetImageByID(id int) *model.Images
}
