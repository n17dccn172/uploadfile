package driver

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)
//const (
//	host     = "postgres"
//	port     = "8080"
//	user     = "postgres"
//	password = "123456"
//	dbname   = "postgres"
//)

var DB *sql.DB
func Connect() *sql.DB{
	//conn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",host, port, user, password, dbname)
	conn := "postgres://postgres:123456@db:5432/postgres?sslmode=disable"
	db, err := sql.Open("postgres", conn)

	if err != nil {
		fmt.Printf("Fail to openDB: %v \n", err)
	}
	DB = db
	return DB
}
