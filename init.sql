CREATE TABLE public.images (
    idimage integer NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    owner text,
    file bytea
);