package main

import (
	"uploadimages/api"
	"uploadimages/driver"
	"fmt"
	"log"
	"net/http"
)

func main(){
	driver.Connect()
	r := http.NewServeMux()
	r.HandleFunc("/api-upload",api.AuthenMiddleJWT(api.ImagesAPI))
	err :=http.ListenAndServe(":5000",r)
	if err!=nil {
		log.Fatal(err)
	}
	fmt.Println("Server listen on port 5000")
}
